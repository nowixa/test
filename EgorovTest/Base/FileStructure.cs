﻿using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace EgorovTest.Base
{
    public struct FileStructure
    {
        /// <summary>
        /// Command line arguments
        /// </summary>
        public string CommandArgs { get; private set; }

        /// <summary>
        /// Application icon
        /// </summary>
        public ImageSource AppIcon
        {
            get
            {
                if (!File.Exists(AppFullPath))
                    return null;

                var icon = Icon.ExtractAssociatedIcon(AppFullPath);
                using (Bitmap bmp = icon.ToBitmap())
                {
                    var stream = new MemoryStream();
                    bmp.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                    return BitmapFrame.Create(stream);
                }
            }
        }

        /// <summary>
        /// Returns the file name and extension
        /// </summary>
        public string AppName { get => Path.GetFileName(AppFullPath); }
        /// <summary>
        /// Returns the file path
        /// </summary>
        public string AppPath { get => Path.GetDirectoryName(AppFullPath); }
        /// <summary>
        /// Returns the absolute path with file name
        /// </summary>
        public string AppFullPath { get; private set; }
        /// <summary>
        /// Search metod: startup user folder or registry.
        /// </summary>
        public TypeOfPath TypeOfPath { get; private set; }

        /// <summary>
        /// Split path string containing command-line parameters
        /// </summary>
        /// <param name="path">Path string to parse containing command-line parameters</param>
        /// <returns>
        /// string[0] - path to file
        /// string[0] - command-line parameters
        /// </returns>
        static string[] ParsePath(string path)
        {
            int inQuote = 0;
            bool inDot = false;

            if (string.IsNullOrEmpty(path))
                return new string[] { path, string.Empty };

            for (int index = 0; index < path.Length; index++)
            {
                if (path[index] == '"')
                    inQuote++;
                if (path[index] == '.')
                    inDot = true;

                if (inQuote == 2 || inDot && path[index] == ' ')
                {
                    return new string[] 
                    { 
                        path.Substring(inQuote > 0 ? 1 : 0, index - (inQuote > 0 ? 1 : 0)),
                        inQuote == 1 && path.Last() == '\"' ? path.Substring(index + 1, path.Length - index - 2) : path.Substring(index + 1)
                    };
                }
            }
            return new string[] { path, string.Empty };
        }

        /// <summary>
        /// Structure of autostart file
        ///     Contain full path to file, command-line parameters and icon
        /// </summary>
        /// <param name="fullPath">Full path to file</param>
        /// <param name="commandArgs">command-line parameters</param>
        /// <param name="pathType">Search method</param>
        public FileStructure(string fullPath, string commandArgs, TypeOfPath pathType)
        {
            CommandArgs = commandArgs;
            var parsePath = ParsePath(fullPath);
            AppFullPath = parsePath[0];
            CommandArgs = parsePath[1];
            TypeOfPath = pathType;
        }
    }
}
