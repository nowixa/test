﻿
namespace EgorovTest.Base
{
    
    /// <summary>
    /// Pair of Path and PathOfType
    /// </summary>
    public struct PathTypePair
    {
        /// <summary>
        /// File search path
        /// </summary>
        public string Path { get; private set; }
        /// <summary>
        /// File search path type
        /// </summary>
        public TypeOfPath PathType { get; private set; }

        /// <summary>
        /// Pair of Path and PathOfType
        /// </summary>
        /// <param name="path">File search path</param>
        /// <param name="pathType">File search path type</param>
        public PathTypePair(string path, TypeOfPath pathType)
        {
            Path = path;
            PathType = pathType;
        }
    }
}
