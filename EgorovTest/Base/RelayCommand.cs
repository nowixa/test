﻿using System;
using System.Windows.Input;

namespace EgorovTest.Base
{
    /// <summary>
    /// ICommand Method
    /// Support CanExecutePropertyChanged notification.
    /// </summary>
    public sealed class RelayCommand : ICommand
    {
        #region Private Properties
        readonly Action<object> _execute;
        readonly Predicate<object> _canExecute;
        #endregion //Private Properties

        #region Constructors

        /// <summary>
        /// Create empty RelayCommand
        /// </summary>
        public RelayCommand()
        {
        }

        /// <summary>
        /// Create RelayCommand
        /// </summary>
        /// <param name="execute">Action to execute when run ICommand.Execute</param>
        public RelayCommand(Action<object> execute)
            : this(execute, null)
        {
        }

        /// <summary>
        /// Create RelayCommand
        /// </summary>
        /// <param name="execute">Action to execute when run ICommand.Execute</param>
        /// <param name="canExecute">true if enabled ICommand.Execute</param>
        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            if (execute == null)
                throw new ArgumentNullException("execute");

            _execute = execute;
            _canExecute = canExecute;
        }
        #endregion // Constructors

        #region ICommand Members
        /// <summary>
        /// Method that determines whether the command can execute in its current state.
        /// </summary>
        /// <param name="parameter">Data used by the command.</param>
        /// <returns></returns>
        public bool CanExecute(object parameter)
        {
            return _canExecute == null ? true : _canExecute(parameter);
        }
        /// <summary>
        /// Method to be called when the command is invoked.
        /// </summary>
        /// <param name="parameter">Data used by the command.</param>
        public void Execute(object parameter)
        {
            _execute?.Invoke(parameter);
        }
        #endregion //ICommand Members

        #region CanExecutePropertyChanged
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
        /// <summary>
        /// Notify that CanExecute changed
        /// </summary>
        public void RaiseCanExecuteChanged()
        {
            CommandManager.InvalidateRequerySuggested();
        }
        #endregion //CanExecutePropertyChanged

    }
}
