﻿
namespace EgorovTest.Base
{
    /// <summary>
    /// Path type
    /// </summary>
    public enum TypeOfPath
    {
        /// <summary>
        /// File system path
        /// </summary>
        Path,
        /// <summary>
        /// Registry path
        /// </summary>
        Registry
    }
}
