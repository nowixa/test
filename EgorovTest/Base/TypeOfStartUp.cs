﻿using System;

namespace EgorovTest.Base
{
    [Flags]
    public enum TypeOfStartUp
    {
        /// <summary>
        /// Search files in registry
        /// </summary>
        Registry    = 1 << 0,
        /// <summary>
        /// Search files in autostart user path
        /// </summary>
        StartMenu   = 1 << 1
    }
}
