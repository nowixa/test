﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.IO;
using EgorovTest.Base;
using Microsoft.Win32;

namespace EgorovTest.SearchThread
{
    /// <summary>
    /// File searcher class
    /// </summary>
    public sealed class FileSearcher
    {
        struct ThreadVariables
        {
            /// <summary>
            /// Event fire when thread start up.
            /// </summary>
            public AutoResetEvent ThreadStarted { get; set; }
            /// <summary>
            /// Search path
            /// </summary>
            public PathTypePair SearchPath { get; set; }
            /// <summary>
            /// File search pattern. Default search all files.
            /// </summary>
            public string FileSearchPattern { get; set; }
            /// <summary>
            /// Action for found file.
            /// </summary>
            public Action<string, TypeOfPath> File { get; set; }
            /// <summary>
            /// Action execute when thread complete.
            /// </summary>
            public Action<int> Complete { get; set; }
            /// <summary>
            /// thread index.
            /// </summary>
            public int Index { get; set; }
            /// <summary>
            /// If function return true search thread is stoped.
            /// </summary>
            public Func<bool> IsNeedStop { get; set; }
        }
        
        #region Private Variables
        private const int WAIT_THREAD = 10000;
        
        private object _lock = new object();
        private Thread[] _threads = null;
        private volatile bool _isNeedStop = false;
        #endregion //Private Variables

        #region Public Properties
        /// <summary>
        /// A EventHandler for when file found.
        /// </summary>
        public event EventHandler<FileSearcherEventArgs> AddFile;
        /// <summary>
        /// A EventHandler for when search completed.
        /// </summary>
        public event EventHandler Complete;
        #endregion //Public Properties

        #region Public Properties
        /// <summary>
        /// Run search file.
        /// </summary>
        /// <param name="searchPathPair">Array of path for search</param>
        public void Start(PathTypePair[] searchPathPair)
        {
            if (searchPathPair == null)
                throw new ArgumentNullException();

            lock (_lock)
            {
                if (_threads != null)
                    throw new InvalidOperationException("Search is already running");

                _isNeedStop = false;

                _threads = new Thread[searchPathPair.Length];

                for (int i = 0; i < _threads.Length; i++)
                {
                    _threads[i] = new Thread(SearchThread);
                    ThreadVariables threadParam = new ThreadVariables() 
                    { 
                        ThreadStarted = new AutoResetEvent(false),
                        SearchPath = searchPathPair[i],
                        File = DoFileFind,
                        Complete = ThreadComplete,
                        Index = i,
                        IsNeedStop = () => { return IsNeedStop; }
                    };
                    _threads[i].Start(threadParam);
                    if (!threadParam.ThreadStarted.WaitOne(WAIT_THREAD))
                        throw new InvalidOperationException("Timeout start search thread");
                }
            }
        }
        /// <summary>
        /// Stop for search file thread.
        /// </summary>
        public void Stop()
        {

            lock(_lock)
            {
                if (_threads == null)
                    return;

                _isNeedStop = true;
            }
        }
        #endregion //Public Properties

        #region Constructor
        public FileSearcher()
        {
            
        }
        #endregion //Constructor

        #region Private Methods

        private static IEnumerable<string> GetFiles(PathTypePair searchPath, string searchPattern)
        {
            switch (searchPath.PathType)
            {
                case TypeOfPath.Path:
                    {
                        return Directory.GetFiles(searchPath.Path,
                        string.IsNullOrEmpty(searchPattern) ? "*.*" : searchPattern, SearchOption.AllDirectories).Where(file => !file.EndsWith(".ini"));
                    }

                case TypeOfPath.Registry:
                    {
                        var idx = searchPath.Path.IndexOfAny(new char[] { '\\', '/' });
                        var regTree = searchPath.Path.Substring(0, idx++).ToUpper();
                        RegistryKey reg;
                        switch (regTree)
                        {
                            case "HKEY_CURRENT_USER":
                                reg = Registry.CurrentUser.OpenSubKey(searchPath.Path.Substring(idx));
                                break;
                            case "HKEY_LOCAL_MACHINE":
                                reg = Registry.LocalMachine.OpenSubKey(searchPath.Path.Substring(idx));
                                break;
                            default:
                                throw new ArgumentException("Don't support regidtry path: {0}", regTree);

                        }
                        return reg.GetValueNames().Select(name => (string)reg.GetValue(name));
                    }
                default:
                    throw new ArgumentException(string.Format("Path type '{0}' is not supported", searchPath.PathType));

            }
        }

        private static void SearchThread(object param)
        {
            if (param == null)
                return;

            var context = (ThreadVariables)param;
            if (context.ThreadStarted == null)
                return;

            try
            {
                context.ThreadStarted.Set();

                if ( string.IsNullOrEmpty(context.SearchPath.Path))
                    return;


                foreach(var file in GetFiles(context.SearchPath, context.FileSearchPattern))
                {
                    if (context.IsNeedStop())
                        break;

                    context.File?.Invoke(file, context.SearchPath.PathType);
                }
            }
            finally
            {
                context.Complete?.Invoke(context.Index);
            }
        }

        private void DoFileFind(string path, TypeOfPath pathType)
        {
            var addFileEvent = AddFile;
            addFileEvent?.Invoke(this, new FileSearcherEventArgs( new Base.FileStructure(path, "", pathType)));
        }

        private void ThreadComplete(int index)
        {
            bool isNeedFireEvent = false;
            lock (_lock)
            {
                _threads[index] = null;
                if (_threads.All(x => x == null))
                {
                    _threads = null;
                    isNeedFireEvent = true;
                }
            }
            if (isNeedFireEvent)
            {
                var completeEvent = Complete;
                completeEvent?.Invoke(this, new EventArgs());
            }
        }

        private bool IsNeedStop { get => _isNeedStop; }

        #endregion //Private Methods

    }
}
