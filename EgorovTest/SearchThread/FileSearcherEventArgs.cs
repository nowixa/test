﻿using EgorovTest.Base;
using System;

namespace EgorovTest.SearchThread
{
    public class FileSearcherEventArgs : EventArgs
    {
        public FileStructure File { get; private set; }

        public FileSearcherEventArgs(FileStructure file) : base()
        {
            File = file;
        }
    }
}
