﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using EgorovTest.Base;
using EgorovTest.SearchThread;

namespace EgorovTest.ViewModel
{
    class MainWindowViewModel : INotifyPropertyChanged
    {
        #region Private Variables

        private static readonly PathTypePair[] _startupList = new PathTypePair[] {
            new PathTypePair( Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    "Microsoft\\Windows\\Start Menu\\Programs\\Startup"), TypeOfPath.Path),
            new PathTypePair( Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData),
                    "Microsoft\\Windows\\Start Menu\\Programs\\Startup"), TypeOfPath.Path),
            new PathTypePair("HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", TypeOfPath.Registry),
            new PathTypePair("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", TypeOfPath.Registry)
        };


        private bool _canStartSearch = true;
        private bool _canStopSearch = false;
        private FileSearcher _fileSearcher;
        #endregion //Private Variables

        #region Public Properties
        public TypeOfStartUp SearchMode { get; set; }

        public ObservableCollection<FileStructure> FileList { get; private set; }

        public IEnumerable<TypeOfStartUp> SearchModeList 
        { 
            get => Enum.GetValues(typeof(TypeOfStartUp)).Cast<TypeOfStartUp>()
                .Append( TypeOfStartUp.Registry | TypeOfStartUp.StartMenu); 
        }

        public ICommand StartSearch { get; private set; }

        public ICommand StopSearch { get; private set; }

        public bool CanStartSearch {
            get => _canStartSearch;
            private set
            {
                if (_canStartSearch == value)
                    return;
                
                _canStartSearch = value;
                RaisePropertyChanged();
            } 
        }
        public bool CanStopSearch { 
            get => _canStopSearch; 
            private set
            {
                if (_canStopSearch == value)
                    return;

                _canStopSearch = value;
                RaisePropertyChanged();
            }
        }

        #endregion //Public Properties

        #region Constructor
        public MainWindowViewModel()
        {
            FileList = new ObservableCollection<FileStructure>();
            StartSearch = new RelayCommand( (param) => DoStartSearch(), (param) => CanStartSearch );
            StopSearch = new RelayCommand( (param) => DoStopSearch(), (param) => CanStopSearch );
            SearchMode = TypeOfStartUp.Registry | TypeOfStartUp.StartMenu;

            _fileSearcher = new FileSearcher();
            _fileSearcher.AddFile += DoAddFileEventHandler;
            _fileSearcher.Complete += DoCompleteSearch;
        }

        #endregion //Constructor

        #region Private Methods
        private void DoStartSearch()
        {
            CanStartSearch = false;
            CanStopSearch = true;
            FileList.Clear();

            _fileSearcher.Start(_startupList.Where(x => TestPathType(SearchMode, x.PathType)).ToArray());
        }
        private void DoStopSearch()
        {
            CanStartSearch = true;
            CanStopSearch = false;

            _fileSearcher.Stop();
        }

        private void DoAddFileEventHandler(object sender, FileSearcherEventArgs e)
        {
            Application.Current?.Dispatcher?.Invoke(new Action(() => FileList.Add( e.File )));
        }
        private void DoCompleteSearch(object sender, EventArgs e)
        {
            DoStopSearch();
        }

        private static bool TestPathType(TypeOfStartUp searchParam, TypeOfPath pathType)
        {
            switch (pathType)
            {
                case TypeOfPath.Path:
                    return (searchParam & TypeOfStartUp.StartMenu) > 0;
                case TypeOfPath.Registry:
                    return (searchParam & TypeOfStartUp.Registry) > 0;
                default:
                    throw new ArgumentException();
            }
        }
        #endregion //Private Methods

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void RaisePropertyChanged([CallerMemberName]string propertyName = "")
        {
            var savedEvent = PropertyChanged;
            savedEvent?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion //INotifyPropertyChanged

    }
}
