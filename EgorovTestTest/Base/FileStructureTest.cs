﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EgorovTest.Base;
using System.Collections.Generic;

namespace EgorovTestTest.Base
{
    [TestClass]
    public class FileStructureTest
    {
        [DynamicData("TestDataString")]
        [DataTestMethod]
        public void TestParsePath(string[] value)
        {
            var parsed = new FileStructure(value[0], "", TypeOfPath.Registry);
            Assert.IsTrue(parsed.AppFullPath == value[1]);
            if (value.Length > 2)
                Assert.IsTrue(parsed.CommandArgs == value[2]);
            else
                Assert.IsTrue(parsed.CommandArgs == "");
        }

        public static IEnumerable<object[]> TestDataString
        {
            get => new[] {
                new object[] { new string[] { null, null } },
                new object[] { new string[] { "", "" } },
                new object[] { new string[] { "a", "a", "" } },
                new object[] { new string[] { "a.exe", "a.exe", "" } },
                new object[] { new string[] { "a.exe -asdf", "a.exe", "-asdf" } },
                new object[] { new string[] { "\"\"", "" } },
                new object[] { new string[] { "\"a", "\"a" } },
                new object[] { new string[] { "\"a.exe\"", "a.exe" } },
                new object[] { new string[] { "\"a.exe -asdf\"", "a.exe", "-asdf" } },
                new object[] { new string[] { "\"a.exe\" -asdf", "a.exe", " -asdf" } },
                new object[] { new string[] { "\"a\" -asdf", "a", " -asdf" } }
            };
        }

    }
}
