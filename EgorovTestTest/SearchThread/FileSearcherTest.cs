﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EgorovTest.SearchThread;
using EgorovTest.Base;
using System.IO;

namespace EgorovTestTest.SearchThread
{
    [TestClass]
    public class FileSearcherTest
    {
        [TestMethod]
        [DataRow(null)]
        public void TestStartNull(PathTypePair[] param)
        {
            var searcher = new FileSearcher();
            try
            {
                searcher.Start(param);
                Assert.Fail();
            }
            catch { 
            }
        }
        [TestMethod]
        public void TestStartRegistry()
        {
            var searcher = new FileSearcher();
            searcher.Start(new PathTypePair[] { new PathTypePair("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", TypeOfPath.Registry) });
        }
        [TestMethod]
        public void TestStartRegistryFail()
        {
            var searcher = new FileSearcher();
            try
            {
                searcher.Start(new PathTypePair[] { new PathTypePair("AAAA\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", TypeOfPath.Registry) });
                Assert.Fail();
            }
            catch
            {
                
            }
        }
        [TestMethod]
        public void TestStartAutoStartPath()
        {
            var searcher = new FileSearcher();
            searcher.Start(new PathTypePair[] { new PathTypePair( Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData),
                    "Microsoft\\Windows\\Start Menu\\Programs\\Startup"), TypeOfPath.Path) });
        }
        [TestMethod]
        public void TestStartAutoStartPathFail()
        {
            var searcher = new FileSearcher();
            try
            {
                searcher.Start(new PathTypePair[] { new PathTypePair( Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData),
                    "Microsoft\\Windows\\Start Menu\\Programs\\xxStartup"), TypeOfPath.Path) });
                Assert.Fail();
            }
            catch
            {

            }
        }

        [TestMethod]
        public void TestStop()
        {
            var searcher = new FileSearcher();
            searcher.Stop();
        }
    }
}
